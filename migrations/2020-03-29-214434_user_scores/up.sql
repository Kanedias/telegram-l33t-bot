-- Your SQL goes here
CREATE TABLE scores(
    `id` INTEGER PRIMARY KEY NOT NULL,
    `user_id` BIGINT NOT NULL,
    `chat_id` BIGINT NOT NULL,
    `message` TEXT NOT NULL,
    `message_timestamp` TIMESTAMP NOT NULL, -- UTC
    `score` BIGINT NOT NULL,

    FOREIGN KEY (`user_id`) REFERENCES users (`id`) 
)