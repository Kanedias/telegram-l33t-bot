use chrono::prelude::*;
use diesel::*;

use super::schema::{scores, users};

#[derive(Identifiable, Queryable, AsChangeset, Hash, PartialEq, Eq)]
#[table_name = "users"]
pub struct UserInfo {
    pub id: i64,
    pub username: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
    pub total_score: i64,
    pub timezone: Option<String>,
}

#[derive(Debug, Identifiable, Queryable, AsChangeset, Associations)]
#[belongs_to(UserInfo, foreign_key = "user_id")]
#[table_name = "scores"]
pub struct ScoreInfo {
    pub id: i32,
    pub user_id: i64,
    pub chat_id: i64,
    pub message: String,
    pub message_timestamp: NaiveDateTime,
    pub score: i64,
}

#[derive(Insertable)]
#[table_name = "users"]
pub struct NewUser {
    id: i64,
    username: String,
    created_at: NaiveDateTime,
    updated_at: NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "scores"]
pub struct NewScore {
    user_id: i64,
    chat_id: i64,
    message: String,
    message_timestamp: NaiveDateTime,
    score: i64,
}

impl UserInfo {
    pub fn new(user_id: i64, username: &str) -> Self {
        return UserInfo {
            id: user_id,
            username: username.to_string(),
            created_at: Local::now().naive_utc(),
            updated_at: Local::now().naive_utc(),
            total_score: 0,
            timezone: None,
        };
    }
}

impl NewScore {
    pub fn new(user_id: i64, chat_id: i64, text: &str, sent_time: NaiveDateTime, win: i64) -> NewScore {
        NewScore {
            user_id,
            chat_id,
            message: text.to_string(),
            message_timestamp: sent_time,
            score: win,
        }
    }
}

impl From<&UserInfo> for NewUser {
    fn from(user_info: &UserInfo) -> Self {
        NewUser {
            id: user_info.id,
            username: user_info.username.to_string(),
            created_at: user_info.created_at,
            updated_at: user_info.updated_at,
        }
    }
}
