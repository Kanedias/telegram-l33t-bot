table! {
    scores (id) {
        id -> Integer,
        user_id -> BigInt,
        chat_id -> BigInt,
        message -> Text,
        message_timestamp -> Timestamp,
        score -> BigInt,
    }
}

table! {
    users (id) {
        id -> BigInt,
        username -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
        total_score -> BigInt,
        timezone -> Nullable<Text>,
    }
}

joinable!(scores -> users (user_id));

allow_tables_to_appear_in_same_query!(scores, users,);
