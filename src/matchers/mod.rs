mod elite;
mod increasing;
mod repeating;

pub static DEFAULT_MATCH_ROUTINES: &[fn(&str) -> i64] =
    &[increasing::match_increasing, repeating::match_repeating, elite::match_elite];
