/// function that considers attempt to be an repeating sequence of numbers
pub fn match_repeating(time_str: &str) -> i64 {
    // attempt example: 20200102222222: skip 4 for year, 4 for month and day
    let attempt_char = time_str.chars().nth(8).unwrap();
    let time_hhmm_str = &time_str[8..12];
    if !time_hhmm_str.chars().all(|c| c == attempt_char) {
        return 0;
    }

    let mut highest_sequence_size = 4;
    for inc_idx in 12..14 {
        let next_last_seq = time_str.chars().nth(inc_idx).expect("Must be a string of 14 digits");
        if next_last_seq == attempt_char {
            highest_sequence_size += 1;
        } else {
            break;
        }
    }

    for dec_idx in (0..8).rev() {
        let next_first_seq = time_str.chars().nth(dec_idx).expect("Must be a string of 14 digits");
        if next_first_seq == attempt_char {
            highest_sequence_size += 1;
        } else {
            break;
        }
    }

    return highest_sequence_size;
}

#[cfg(test)]
mod tests {
    use super::match_repeating;

    #[test]
    fn test_repeating_positive() {
        assert_eq!(match_repeating("20200101222200"), 4);
        assert_eq!(match_repeating("20191231222220"), 5);
        assert_eq!(match_repeating("20070120222222"), 6);

        assert_eq!(match_repeating("20200304111100"), 4);
        assert_eq!(match_repeating("20200122111110"), 5);
        assert_eq!(match_repeating("20200121111110"), 6);

        assert_eq!(match_repeating("20200120111100"), 4);
        assert_eq!(match_repeating("20200120111122"), 4);
        assert_eq!(match_repeating("20200120222211"), 4);

        assert_eq!(match_repeating("20770122222222"), 8);
        assert_eq!(match_repeating("20201111111111"), 10);
        assert_eq!(match_repeating("20211111111111"), 11);
        assert_eq!(match_repeating("21111111111111"), 13);
    }

    #[test]
    fn test_repeating_negative() {
        assert_eq!(match_repeating("20190511223100"), 0);
        assert_eq!(match_repeating("20071121113500"), 0);
        assert_eq!(match_repeating("20200101221145"), 0);
    }
}
