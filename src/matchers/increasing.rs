/// function that considers attempt to be an increasing sequence of numbers
pub fn match_increasing(time_str: &str) -> i64 {
    // attempt example: 20200102133700: skip 4 for year, 4 for month and day
    let time_hhmm_str = &time_str[8..12];
    if !PERMITTED_INCR_HOUR_SEQUENCES.contains(&time_hhmm_str) {
        return 0;
    }

    // now that we know the hour is right, let's count how many we scored here
    let mut highest_sequence_size = 4;
    let attempt_last_seq = time_hhmm_str.chars().last().expect("Must be a string of 14 digits");
    let mut attempt_last_seq = attempt_last_seq.to_digit(10).unwrap() as i32;
    for inc_idx in 12..14 {
        let next_last_seq = time_str.chars().nth(inc_idx).expect("Must be a string of 14 digits");
        let next_last_seq = next_last_seq.to_digit(10).unwrap() as i32;
        if next_last_seq - attempt_last_seq == 1 {
            attempt_last_seq = next_last_seq;
            highest_sequence_size += 1;
        } else {
            // non-increasing
            break;
        }
    }

    let attempt_first_seq = time_hhmm_str.chars().nth(0).expect("Attempt must be a string of 14 digits");
    let mut attempt_first_seq = attempt_first_seq.to_digit(10).unwrap() as i32;
    for dec_idx in (0..8).rev() {
        let next_first_seq = time_str.chars().nth(dec_idx).expect("Must be a string of 14 digits");
        let next_first_seq = next_first_seq.to_digit(10).unwrap() as i32;
        if attempt_first_seq - next_first_seq == 1 {
            attempt_first_seq = next_first_seq;
            highest_sequence_size += 1;
        } else {
            // non-decreasing
            break;
        }
    }

    return highest_sequence_size;
}

const PERMITTED_INCR_HOUR_SEQUENCES: &[&str] = &[
    "0123", // 01:23
    "1234", // 12:34
    "2345", // 23:45
];

#[cfg(test)]
mod tests {
    use super::match_increasing;

    #[test]
    fn test_increasing_positive() {
        assert_eq!(match_increasing("20200101123400"), 4);
        assert_eq!(match_increasing("20200101123450"), 5);
        assert_eq!(match_increasing("20200101123456"), 6);

        assert_eq!(match_increasing("20200304234500"), 4);
        assert_eq!(match_increasing("20200111234500"), 5);
        assert_eq!(match_increasing("20200101234500"), 6);

        assert_eq!(match_increasing("20200101012300"), 4);
        assert_eq!(match_increasing("20200101012340"), 5);
        assert_eq!(match_increasing("20200101012345"), 6);
    }

    #[test]
    fn test_increasing_negative() {
        assert_eq!(match_increasing("20190511103400"), 0);
        assert_eq!(match_increasing("20110521133700"), 0);
        assert_eq!(match_increasing("20071121234000"), 0);
        assert_eq!(match_increasing("20100125130150"), 0);
    }
}
