/// function that considers attempt to be an simple elite statement
pub fn match_elite(time_str: &str) -> i64 {
    // attempt example: 20200102133700: skip 4 for year, 4 for month and day
    let time_hhmm_str = &time_str[8..12];
    if !PERMITTED_ELITE_SEQUENCES.contains(&time_hhmm_str) {
        return 0;
    }

    return 4;
}

const PERMITTED_ELITE_SEQUENCES: &[&str] = &[
    "1337", // 13:37
];

#[cfg(test)]
mod tests {
    use super::match_elite;

    #[test]
    fn test_elite_positive() {
        assert_eq!(match_elite("20200101133700"), 4);
        assert_eq!(match_elite("20191231133750"), 4);
        assert_eq!(match_elite("20070120133726"), 4);
    }

    #[test]
    fn test_increasing_negative() {
        assert_eq!(match_elite("20190511103400"), 0);
        assert_eq!(match_elite("20071121234000"), 0);
        assert_eq!(match_elite("20200101012345"), 0);
    }
}
