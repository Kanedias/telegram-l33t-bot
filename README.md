Telegram L33T Bot
=================

This is a simple bot that gives you a high-five when
you write `1337` in 13:37.

Features:
* A high-five! (obviously)
* Timezone awareness
* Cleans its own messages after 1 minute
* Combines all scores into summary post after 1 minute
* Shows hint if you are in another timezone and trying to hit a score
* Tracks all scores in a database to show high scores across all participants!

WIP:
* Showing stats for chat
* Showing stats for user

Compiling
---------

This is a rust project, so usual rules apply:

```bash
$ # setup rustup: have cargo/rustc commands in your PATH
$ cargo build --release
```

Running
-------

As this is a Telegram bot project, first you need to 
get your bot token from `@BotFather` Telegram account.
After this is done, run it like this:

```bash
$ export TELEGRAM_BOT_TOKEN=<YOUR_BOT_TOKEN_HERE>
$ cargo run --release
```

License
--------

    Copyright (C) 2020 Oleg `Kanedias` Chernovskiy
    Copyright (C) 2020 Phil `Sorseg` Samoylov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
